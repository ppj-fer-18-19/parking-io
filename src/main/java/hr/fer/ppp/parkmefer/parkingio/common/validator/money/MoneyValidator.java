package hr.fer.ppp.parkmefer.parkingio.common.validator.money;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class MoneyValidator implements ConstraintValidator<Money, Object> {

    private boolean positiveAmount;

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        if (value instanceof Number) {
            double doubleValue = ((Number) value).doubleValue();

            if (positiveAmount) return doubleValue >= 0;
            else return doubleValue <= 0;
        }

        return false;
    }

    @Override
    public void initialize(Money constraintAnnotation) {
        this.positiveAmount = constraintAnnotation.positiveAmount();
    }
}
