package hr.fer.ppp.parkmefer.parkingio.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

@Configuration
@EnableSwagger2
public class SwaggerConfig extends WebMvcConfigurationSupport {

    @Bean
    public Docket productApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("hr.fer.ppp.parkmefer.parkingio.controllers"))
                .paths(PathSelectors.any())
                .build().apiInfo(apiInfo());

    }

    @Override
    protected void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("swagger-ui.html")
                .addResourceLocations("classpath:/META-INF/resources/");

        registry.addResourceHandler("/webjars/**")
                .addResourceLocations("classpath:/META-INF/resources/webjars/");
    }

    private ApiInfo apiInfo() {
        return new ApiInfo(
                "ParkMe-FER - parking-io",
                "Restful microservice which is used for monitoring parking spaces and managing and providing parking information..",
                "v1-DEVELOPMENT",
                "https://www.fer.unizg.hr/predmet/pro",
                new Contact("Darko Britvec", "www.linkedin.com/in/darko-britvec-ab4761172", "darko.britvec@fer.hr"),
                "", "", Collections.emptyList());
    }

    //TODO add oauth2 documentation and custom comments
    // https://springframework.guru/spring-boot-restful-api-documentation-with-swagger-2/
    // https://www.baeldung.com/swagger-2-documentation-for-spring-rest-api
}