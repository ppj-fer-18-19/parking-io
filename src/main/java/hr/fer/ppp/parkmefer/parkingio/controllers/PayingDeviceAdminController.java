package hr.fer.ppp.parkmefer.parkingio.controllers;

import hr.fer.ppp.parkmefer.parkingio.dto.PayingDeviceDTO;
import hr.fer.ppp.parkmefer.parkingio.dto.ResponseDTO;
import hr.fer.ppp.parkmefer.parkingio.services.PayingDeviceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.UUID;

@RestController
@RequestMapping(path = "admin/payingDevice")
public class PayingDeviceAdminController extends AbstractController {

    private PayingDeviceService payingDeviceService;

    @Autowired
    public PayingDeviceAdminController(MessageSource messageSource, PayingDeviceService payingDeviceService) {
        super(messageSource);

        this.payingDeviceService = payingDeviceService;
    }

    @GetMapping(path = "/{id}", produces = "application/json")
    public ResponseEntity<PayingDeviceDTO> getPayingDeviceById(@PathVariable(name = "id") UUID id) {
        return ResponseEntity.ok(payingDeviceService.getPayingDeviceById(id));
    }

    @PostMapping(path = "/register", produces = "application/json")
    public ResponseEntity<ResponseDTO> registerPayingDevice(@RequestBody @Valid PayingDeviceDTO parkingLotDTO, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return ResponseEntity.unprocessableEntity().body(createResponseDTO(bindingResult));
        }

        payingDeviceService.registerPayingDevice(parkingLotDTO);
        return ResponseEntity.ok(createResponseDTO());
    }

    @PutMapping(path = "/edit", produces = "application/json")
    public ResponseEntity<ResponseDTO> editPayingDevice(@RequestBody @Valid PayingDeviceDTO parkingLotDTO, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return ResponseEntity.unprocessableEntity().body(createResponseDTO(bindingResult));
        }

        payingDeviceService.editPayingDevice(parkingLotDTO);
        return ResponseEntity.ok(createResponseDTO());
    }

    @DeleteMapping(path = "/delete", produces = "application/json")
    public ResponseEntity<ResponseDTO> deletePayingDevice(@RequestParam UUID id) {
        payingDeviceService.deletePayingDevice(id);

        return ResponseEntity.ok(createResponseDTO());
    }
}
