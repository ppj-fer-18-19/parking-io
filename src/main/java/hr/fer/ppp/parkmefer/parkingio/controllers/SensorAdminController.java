package hr.fer.ppp.parkmefer.parkingio.controllers;

import hr.fer.ppp.parkmefer.parkingio.dto.ResponseDTO;
import hr.fer.ppp.parkmefer.parkingio.dto.SensorDTO;
import hr.fer.ppp.parkmefer.parkingio.services.SensorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.UUID;

@RestController
@RequestMapping(path = "admin/sensor")
public class SensorAdminController extends AbstractController {

    private SensorService sensorService;

    @Autowired
    public SensorAdminController(MessageSource messageSource, SensorService sensorService) {
        super(messageSource);

        this.sensorService = sensorService;
    }

    @GetMapping(path = "/{id}", produces = "application/json")
    public ResponseEntity<SensorDTO> getSensorById(@PathVariable(name = "id") UUID id) {
        return ResponseEntity.ok(sensorService.getSensorById(id));
    }

    @PostMapping(path = "/register", produces = "application/json")
    public ResponseEntity<ResponseDTO> registerSensor(@RequestBody @Valid SensorDTO sensorDTO, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return ResponseEntity.unprocessableEntity().body(createResponseDTO(bindingResult));
        }

        sensorService.registerSensor(sensorDTO);
        return ResponseEntity.ok(createResponseDTO());
    }

    @PutMapping(path = "/edit", produces = "application/json")
    public ResponseEntity<ResponseDTO> editSensor(@RequestBody @Valid SensorDTO sensorDTO, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return ResponseEntity.unprocessableEntity().body(createResponseDTO(bindingResult));
        }

        sensorService.editSensor(sensorDTO);
        return ResponseEntity.ok(createResponseDTO());
    }

    @DeleteMapping(path = "/delete", produces = "application/json")
    public ResponseEntity<ResponseDTO> deleteSensor(@RequestParam UUID id) {
        sensorService.deleteSensor(id);

        return ResponseEntity.ok(createResponseDTO());
    }
}
