package hr.fer.ppp.parkmefer.parkingio.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.UUID;

@Data
public class PayingDeviceDTO implements Serializable {

    private static final long serialVersionUID = -4052026586066315062L;

    private UUID id;

    @JsonProperty(required = true)
    @Min(value = -180, message = "appParkingForm.longitude.min")
    @Max(value = 180, message = "appParkingForm.longitude.max")
    @NotNull(message = "appParkingForm.name.notBlank")
    private Double longitude;

    @JsonProperty(required = true)
    @Min(value = -90, message = "appParkingForm.latitude.min")
    @Max(value = 90, message = "appParkingForm.latitude.max")
    @NotNull(message = "appParkingForm.name.notBlank")
    private Double latitude;

    private String info;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @NotNull(message = "appParkingForm.name.notBlank")
    private UUID parkingId;
}
