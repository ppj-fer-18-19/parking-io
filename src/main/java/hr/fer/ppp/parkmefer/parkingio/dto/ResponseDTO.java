package hr.fer.ppp.parkmefer.parkingio.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@Data
public class ResponseDTO implements Serializable {

    private static final long serialVersionUID = 578848514693279389L;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private Map<String, String> errors = new HashMap<>();

    public void addError(String name, String description) {
        errors.put(name, description);
    }

}
