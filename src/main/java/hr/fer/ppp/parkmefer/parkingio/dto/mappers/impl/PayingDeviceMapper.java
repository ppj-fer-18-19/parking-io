package hr.fer.ppp.parkmefer.parkingio.dto.mappers.impl;

import hr.fer.ppp.parkmefer.parkingio.dto.PayingDeviceDTO;
import hr.fer.ppp.parkmefer.parkingio.dto.mappers.Mapper;
import hr.fer.ppp.parkmefer.parkingio.dto.mappers.Updater;
import hr.fer.ppp.parkmefer.parkingio.entities.PayingDevice;
import org.springframework.stereotype.Component;

@Component
public class PayingDeviceMapper implements Mapper<PayingDevice, PayingDeviceDTO>, Updater<PayingDevice, PayingDeviceDTO> {

    @Override
    public PayingDevice dtoToEntity(PayingDeviceDTO dto) {
        return fillData(new PayingDevice(), dto);
    }

    @Override
    public PayingDeviceDTO entityToDto(PayingDevice entity) {
        PayingDeviceDTO payingDeviceDTO = entityToDtoMinimal(entity);
        payingDeviceDTO.setInfo(entity.getInfo());

        return payingDeviceDTO;
    }

    @Override
    public PayingDeviceDTO entityToDtoMinimal(PayingDevice entity) {
        PayingDeviceDTO payingDeviceDTO = new PayingDeviceDTO();

        payingDeviceDTO.setId(entity.getId());
        payingDeviceDTO.setLongitude(entity.getLongitude());
        payingDeviceDTO.setLatitude(entity.getLatitude());
        payingDeviceDTO.setParkingId(entity.getParkingId());

        return payingDeviceDTO;
    }

    @Override
    public PayingDevice updateEntity(PayingDevice entity, PayingDeviceDTO dto) {
        return fillData(entity, dto);
    }

    private PayingDevice fillData(PayingDevice entity, PayingDeviceDTO dto) {
        entity.setInfo(dto.getInfo());
        entity.setLongitude(dto.getLongitude());
        entity.setLatitude(dto.getLatitude());

        return entity;
    }
}
