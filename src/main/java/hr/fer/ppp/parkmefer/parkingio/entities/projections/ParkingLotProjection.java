package hr.fer.ppp.parkmefer.parkingio.entities.projections;

import java.io.Serializable;
import java.util.UUID;

public interface ParkingLotProjection extends Serializable {

    UUID getId();

    String getName();

    Integer getMaxSpaces();

    Integer getOccupiedSpaces();

    Long getZoneId();

    Boolean getEnabled();

    Double getLongitude();

    Double getLatitude();
}
