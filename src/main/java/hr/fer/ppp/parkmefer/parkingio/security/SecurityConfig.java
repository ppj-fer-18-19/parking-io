package hr.fer.ppp.parkmefer.parkingio.security;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.Resource;
import org.springframework.security.oauth2.provider.token.AccessTokenConverter;
import org.springframework.security.oauth2.provider.token.DefaultAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.UserAuthenticationConverter;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;

@Configuration
public class SecurityConfig {

    @Value("classpath:/security/park-me-fer")
    private Resource privateKey;

    @Value("classpath:/security/park-me-fer.pub")
    private Resource publicKey;

    @Bean
    @Primary
    public DefaultTokenServices tokenServices() {
        final DefaultTokenServices defaultTokenServices = new DefaultTokenServices();
        defaultTokenServices.setTokenStore(tokenStore());
        defaultTokenServices.setSupportRefreshToken(true);
        return defaultTokenServices;
    }

    @Bean
    public TokenStore tokenStore() {
        return new JwtTokenStore(accessTokenConverter());
    }

    @Bean
    public JwtAccessTokenConverter accessTokenConverter() {
        UserAuthenticationConverter userAuthenticationConverter = new AuthToPrincipal();

        AccessTokenConverter accessTokenConverter = new DefaultAccessTokenConverter();
        ((DefaultAccessTokenConverter) accessTokenConverter).setUserTokenConverter(userAuthenticationConverter);
        ((DefaultAccessTokenConverter) accessTokenConverter).setIncludeGrantType(true);

        JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
        converter.setAccessTokenConverter(accessTokenConverter);

        try {
            String privateKeyAsString = new String(Files.readAllBytes(privateKey.getFile().toPath()), StandardCharsets.UTF_8);
            String publicKeyAsString = new String(Files.readAllBytes(publicKey.getFile().toPath()), StandardCharsets.UTF_8);

            converter.setSigningKey(privateKeyAsString);
            converter.setVerifierKey(publicKeyAsString);
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }

        return converter;
    }
}
