package hr.fer.ppp.parkmefer.parkingio.services;

import hr.fer.ppp.parkmefer.parkingio.dto.PayingDeviceDTO;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;

public interface PayingDeviceService {

    /**
     * @param upperLeftLong  longitude of upper left angle of the screen
     * @param upperLeftLat   latitude of upper left angle of the screen
     * @param lowerRightLong longitude of lower right angle of the screen
     * @param lowerRightLat  latitude of lower right angle of the screen
     * @return list of paying devices in desired rectangle
     */
    List<PayingDeviceDTO> getPayingDevicesInRectangle(
            double upperLeftLong, double upperLeftLat, double lowerRightLong, double lowerRightLat);

    /**
     * @param id paying device id
     * @return requested paying device
     * @throws javax.persistence.EntityNotFoundException
     */
    PayingDeviceDTO getPayingDeviceById(@NotNull UUID id);

    /**
     * @param payingDeviceDTO dto with data of paying device to be registered
     * @return id of registered paying device
     */
    UUID registerPayingDevice(@NotNull PayingDeviceDTO payingDeviceDTO);

    /**
     * @param payingDeviceDTO dto with new data of existing paying device
     */
    void editPayingDevice(@NotNull PayingDeviceDTO payingDeviceDTO);

    /**
     * @param id id of paying device to be deleted
     */
    void deletePayingDevice(@NotNull UUID id);
}
