package hr.fer.ppp.parkmefer.parkingio.services;

import hr.fer.ppp.parkmefer.parkingio.dto.SensorDTO;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;

public interface SensorService {

    /**
     * @param upperLeftLong  longitude of upper left angle of the screen
     * @param upperLeftLat   latitude of upper left angle of the screen
     * @param lowerRightLong longitude of lower right angle of the screen
     * @param lowerRightLat  latitude of lower right angle of the screen
     * @return list of sensors in desired rectangle
     */
    List<SensorDTO> getSensorsInRectangle(
            double upperLeftLong, double upperLeftLat, double lowerRightLong, double lowerRightLat);

    /**
     * @param id sensor id
     * @return requested sensor
     * @throws javax.persistence.EntityNotFoundException
     */
    SensorDTO getSensorById(@NotNull UUID id);

    /**
     * @param sensorDTO dto with data of sensor to be registered
     * @return id of registered sensor
     */
    UUID registerSensor(@NotNull SensorDTO sensorDTO);

    /**
     * @param sensorDTO data of existing sensor
     */
    void editSensor(@NotNull SensorDTO sensorDTO);

    /**
     * @param id id of sensor to be deleted
     */
    void deleteSensor(@NotNull UUID id);
}
