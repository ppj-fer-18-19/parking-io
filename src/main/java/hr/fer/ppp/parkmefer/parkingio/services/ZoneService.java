package hr.fer.ppp.parkmefer.parkingio.services;

import hr.fer.ppp.parkmefer.parkingio.dto.ZoneDTO;

import javax.validation.constraints.NotNull;
import java.util.List;

public interface ZoneService {

    /**
     * @return all registered zones
     */
    List<ZoneDTO> getAllZones();

    /**
     * @param id zone id
     * @return requested zone
     * @throws javax.persistence.EntityNotFoundException
     */
    ZoneDTO getZoneById(@NotNull Long id);

    /**
     * @param zoneDTO dto with data of zone to be registered
     * @return id of registered zone
     */
    Long registerZone(@NotNull ZoneDTO zoneDTO);

    /**
     * @param zoneDTO data of existing zone
     */
    void editZone(@NotNull ZoneDTO zoneDTO);

    /**
     * @param id id of zone to be deleted
     */
    void deleteZone(@NotNull Long id);
}
