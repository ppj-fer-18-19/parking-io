package hr.fer.ppp.parkmefer.parkingio.services.impl;

import hr.fer.ppp.parkmefer.parkingio.config.LogConfig;
import hr.fer.ppp.parkmefer.parkingio.dto.ZoneDTO;
import hr.fer.ppp.parkmefer.parkingio.dto.mappers.impl.ZoneMapper;
import hr.fer.ppp.parkmefer.parkingio.entities.Zone;
import hr.fer.ppp.parkmefer.parkingio.repositories.ParkingRepository;
import hr.fer.ppp.parkmefer.parkingio.repositories.ZoneRepository;
import hr.fer.ppp.parkmefer.parkingio.services.ZoneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ZoneServiceImpl extends AbstractService implements ZoneService {

    private final ZoneRepository zoneRepository;
    private final ZoneMapper zoneMapper;

    @Autowired
    public ZoneServiceImpl(MessageSource messageSource, ParkingRepository parkingRepository, ZoneRepository zoneRepository, ZoneMapper zoneMapper) {
        super(parkingRepository, messageSource);

        this.zoneRepository = zoneRepository;
        this.zoneMapper = zoneMapper;
    }

    @Override
    public List<ZoneDTO> getAllZones() {
        return zoneRepository
                .findAll()
                .stream()
                .map(zoneMapper::entityToDto)
                .collect(Collectors.toList());
    }

    @Override
    public ZoneDTO getZoneById(Long id) {
        return zoneMapper.entityToDto(zoneRepository.findById(id).orElseThrow(ENTITY_NOT_FOUND));
    }

    @Override
    public Long registerZone(ZoneDTO zoneDTO) {
        Zone zone = zoneMapper.dtoToEntity(zoneDTO);

        zoneRepository.save(zone);
        log(LogConfig.ZONE_REGISTRATION, zone.getId());

        return zone.getId();
    }

    @Override
    public void editZone(ZoneDTO zoneDTO) {
        Zone zone = zoneRepository.findById(zoneDTO.getId()).orElseThrow(ENTITY_NOT_FOUND);
        zoneMapper.updateEntity(zone, zoneDTO);

        zoneRepository.save(zone);
        log(LogConfig.ZONE_EDIT, zone.getId());
    }

    @Override
    public void deleteZone(Long id) {
        Zone zone = zoneRepository.findById(id).orElseThrow(ENTITY_NOT_FOUND);

        log(LogConfig.ZONE_DELETE, zone.toString());
        zoneRepository.delete(zone);
    }
}
