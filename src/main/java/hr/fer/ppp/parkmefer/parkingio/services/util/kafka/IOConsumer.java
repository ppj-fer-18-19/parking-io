package hr.fer.ppp.parkmefer.parkingio.services.util.kafka;

import com.google.gson.Gson;
import hr.fer.ppp.parkmefer.parkingio.dto.MessageDTO;
import hr.fer.ppp.parkmefer.parkingio.entities.Sensor;
import hr.fer.ppp.parkmefer.parkingio.repositories.SensorRepository;
import hr.fer.ppp.parkmefer.parkingio.services.util.notifications.NotificationSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import javax.validation.Valid;
import java.util.Optional;
import java.util.logging.Logger;

@Component
public class IOConsumer {

    private final Gson gson;
    private final SensorRepository sensorRepository;
    private final NotificationSender notificationSender;

    @Autowired
    public IOConsumer(SensorRepository sensorRepository, Gson gson, NotificationSender notificationSender) {
        this.gson = gson;
        this.sensorRepository = sensorRepository;
        this.notificationSender = notificationSender;
    }

    @KafkaListener(topics = "${app.kafka.topic}")
    public void listenEntries(@Payload @Valid String message) {
        Logger.getAnonymousLogger().info(message);

        MessageDTO messageDTO = gson.fromJson(message, MessageDTO.class);

        Optional<Sensor> sensorOptional = sensorRepository.findById(messageDTO.getSensorId());
        if (sensorOptional.isPresent()) {
            Sensor sensor = sensorOptional.get();

            sensor.setOccupied(messageDTO.getOccupied());
            sensor.setLastUpdated(messageDTO.getTime());

            sensorRepository.save(sensor);
            notificationSender.sendNotification(sensor);
        }
    }
}
