package hr.fer.ppp.parkmefer.parkingio.services.util.notifications;

import com.google.firebase.FirebaseApp;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.Message;
import hr.fer.ppp.parkmefer.parkingio.entities.Sensor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.xml.ws.Response;
import java.util.concurrent.Executor;

@Component
public class NotificationSender {

    private static final Logger LOGGER = LoggerFactory.getLogger(NotificationSender.class);

    private final FirebaseApp firebaseApp;
    private final Executor executor;

    @Autowired
    public NotificationSender(Executor executor, FirebaseApp firebaseApp) {
        this.firebaseApp = firebaseApp;
        this.executor = executor;

    }

    public void sendNotification(Sensor sensor) {
        Message message = Message.builder()
                .putData("id", sensor.getId().toString())
                .putData("occupied", Boolean.toString(sensor.getOccupied()))
                .setTopic(sensor.getParkingId().toString())
                .build();

        FirebaseMessaging.getInstance(firebaseApp).sendAsync(message).addListener(() -> {
            LOGGER.info(String.format("Message sent to topic: %s", sensor.getParkingId().toString()));
        }, executor);
    }
}
