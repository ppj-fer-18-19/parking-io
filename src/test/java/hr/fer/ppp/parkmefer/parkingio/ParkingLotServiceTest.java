package hr.fer.ppp.parkmefer.parkingio;

import hr.fer.ppp.parkmefer.parkingio.common.money.MonetaryAmount;
import hr.fer.ppp.parkmefer.parkingio.dto.ParkingLotDTO;
import hr.fer.ppp.parkmefer.parkingio.dto.PayingDeviceDTO;
import hr.fer.ppp.parkmefer.parkingio.dto.SensorDTO;
import hr.fer.ppp.parkmefer.parkingio.dto.mappers.impl.ParkingLotMapper;
import hr.fer.ppp.parkmefer.parkingio.entities.Zone;
import hr.fer.ppp.parkmefer.parkingio.repositories.ZoneRepository;
import hr.fer.ppp.parkmefer.parkingio.services.ParkingLotService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Currency;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ParkingLotServiceTest {

    @Autowired
    private ParkingLotService parkingLotService;

    @Autowired
    private ParkingLotMapper parkingLotMapper;

    @Autowired
    private ZoneRepository zoneRepository;


    @Test
    @Transactional(value = Transactional.TxType.NEVER)
    public void test1(){
        Zone zone = new Zone();
        zone.setName("A");
        zone.setPhone("+385");
        zone.setPrice(new MonetaryAmount(10.0, Currency.getInstance("HRK")));
        zoneRepository.save(zone);

        ParkingLotDTO dto = new ParkingLotDTO();
        dto.setZoneId(zone.getId());
        dto.setEnabled(true);
        dto.setOccupiedSpaces(10);
        dto.setMaxSpaces(100);
        dto.setLongitude(0.0);
        dto.setLatitude(0.0);
        dto.setName("AAA");

        List<SensorDTO> sensorList = new ArrayList<>();
        for(int i = 0; i < 10; i++){
            SensorDTO sensorDTO = new SensorDTO();
            sensorDTO.setLongitude(0.01);
            sensorDTO.setLatitude(0.01);

            sensorList.add(sensorDTO);
        }

        List<PayingDeviceDTO> payingDeviceList = new ArrayList<>();
        PayingDeviceDTO payingDeviceDTO = new PayingDeviceDTO();
        payingDeviceDTO.setInfo("aaa");
        payingDeviceDTO.setLongitude(0.01);
        payingDeviceDTO.setLatitude(0.02);
        payingDeviceList.add(payingDeviceDTO);

        dto.setSensors(sensorList);
        dto.setPayingDevices(payingDeviceList);

        parkingLotService.registerParkingLot(dto);
    }
}
